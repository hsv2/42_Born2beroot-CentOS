#!/usr/bin/env bash
# vim: et sw=4 ts=4:
if [[ -e /tmp/shutup ]];
then
    exit
fi

ARCH="`uname -m`"
KERNEL_VERSION="`uname -r`"
CPUs="`lscpu | gawk '/Socket\(s\)/ {print $2}'`"
vCPUs="`lscpu | gawk '/^[:space:]*CPU\(s\)/ {print $2}'`"
MEMORY="`free -m | gawk '/Mem:/ {usage = $3 / $2 * 100; printf "Used: %2.2f%% Available: %dM", usage, $7}'`"
DISK="`df -t xfs --total -h --output=size,pcent | tail -1 | gawk '{printf "Usage: %s Size: %s", $2, $1}'`"
CPU_USAGE="`top -bn1 | grep "Cpu(s)" | \
	sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | \
	awk '{print 100 - $1"%"}'`"
BOOT_TIME="`last -1 --time-format iso reboot | gawk 'NR == 1 {print $5}'`"
LVM_STATUS="`lvm vgdisplay -A | gawk '/VG Name/ {printf \"VG %s is active\", $3}'`"
if [[ -z $LVM_STATUS ]];
then
    LVM_STATUS="No volume group active"
fi

ESTABLISHED_CONNECTIONS="`ss | grep -vE '(u_str|Netid)' | wc -l`"
USER_COUNT="`who -q | tail -1 | sed -Ee 's/.*users=([[:digit:]]+)/\1/'`"
HOST_IP="`hostname -i`"
HOST_MAC="`ip -br link show enp0s3 | gawk '{print $3}'`"
SUDO_EXECUTIONS="`grep COMMAND /var/log/sudo/sudo.log | wc -l`"

MSG="Kernel $KERNEL_VERSION for $ARCH booted on $BOOT_TIME"
MSG="$MSG\nSockets/vCPU: $CPUs/$vCPUs"
MSG="$MSG\nCPU usage: $CPU_USAGE"
MSG="$MSG\nMemory: $MEMORY"
MSG="$MSG\nStorage: $DISK"
MSG="$MSG\nLVM status: $LVM_STATUS"
MSG="$MSG\nAddress: $HOST_IP $HOST_MAC"
MSG="$MSG\n\nUsers: $USER_COUNT"
MSG="$MSG\nConnections: $ESTABLISHED_CONNECTIONS"
MSG="$MSG\nSudo executions: $SUDO_EXECUTIONS"

wall --nobanner -t 1 "`echo -e $MSG`"
