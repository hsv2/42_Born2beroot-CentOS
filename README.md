<!-- vim: et ts=4 sw=4:
-->
# Born2beroot - CentOS Stream 9

[![ablanken's 42 Born2beroot Score](https://badge42.vercel.app/api/v2/cl4cer769008809jv6ljzljw9/project/2593788)](https://github.com/JaeSeoKim/badge42)

## Installation

Make the following [kickstart](kickstart-install/Born2beroot.cfg) configuration
file available over HTTP:

```bash
python3 -m http.server --directory kickstart-install
```

When in the installation media boot menu, add the following option to boot
parameters: `inst.ks=http://YOUR_HOST_ADDRESS/ks.cfg` and then boot. The
installation should start and run fully automated now.

See the [kickstart](kickstart-install/Born2beroot.cfg) file for more
informations on the system setup.

## Install required packages

```bash
# To download this repo and lighttpd sources.
dnf install git
# SELinux policy tools.
dnf install policycoreutils-python-utils
# Extra Packages for Entreprise Linux 9 repository in which ufw package is.
dnf install epel-release
# Uncomplicated Firewall.
dnf install ufw
# Dependencies for building lighttpd from sources.
dnf groupinstall "Development Tools"
dnf install git pcre2-devel
# MariaDB and PHP for Bonus.
dnf install mariadb-server php-fpm php-mysqlnd
# FTP server for Bonus.
dnf install vsftpd
```

Enable services:

```bash
systemctl enable mariadb php-fpm ufw --now
```

## Configuration

### Password policy

Set password aging settings in [/etc/login.defs](rootfs/etc/login.defs).

Set password complexity requirements in
[/etc/security/pwquality.conf.d/99-Born2beroot.conf](rootfs/etc/security/pwquality.conf.d/99-Born2beroot.conf)

### sudo

Add the provided [sudoers policy file](rootfs/etc/sudoers.d/Born2beroot) in
`/etc/sudoers.d`.

### Remote access

Disable root SSH login and change listening port adding
[/etc/ssh/sshd_config.d/99-Born2beroot.conf](rootfs/etc/ssh/sshd_config.d/99-Born2beroot.conf)
configuration file.

You also have to edit SELinux policy to allow `sshd` to listen on the
non-default TCP port 4242 and reload service:

```bash
semanage port --add -t ssh_port_t -p tcp 4242
systemctl reload sshd
```

### monitoring.sh

Install [monitoring.sh](rootfs/usr/local/sbin/monitoring.sh)
script in `/usr/local/sbin`,
[monitoring.service](rootfs/etc/systemd/system/monitoring.service)
systemd unit and [monitoring.timer](rootfs/etc/systemd/system/monitoring.timer)
systemd timer in `/etc/systemd/system`.

Now you can enable and start it:

```bash
systemctl daemon-reload
systemctl enable monitoring.timer --now
```

You can disable it temporarly by creating a `/tmp/nowall` file or adding the
kernel boot cmd line parameter `nowall`.

### PHP

Copy PHP configuration files
[/etc/php.d/99-lighttpd.ini](rootfs/etc/php.d/99-lighttpd.ini) and
[/etc/php-fpm.d/www.conf].

The allow `php-fpm` to access the web server web root and then restart it:

```bash
semanage boolean --modify --on httpd_unified
mkdir -p /srv/www/htdocs
semanage fcontext -a -t httpd_sys_content_t /srv/www
systemctl reload php-fpm
```

### MariaDB

Setup MariaDB root account and do a basic securization of it:

```bash
mysql_secure_installation
```

Then enter the MariaDB shell with `mariadb -u root` and create an user for
Wordpress and assign it a table:

```SQL
CREATE DATABASE wordpress;
CREATE USER 'wordpress'@'localhost' IDENTIFIED BY 'CHANGE_ME';
GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'localhost';
FLUSH PRIVILEGES;
```

### Lighttpd

Get lighty source, compile them and install it:

```bash
cd /usr/local/src
git clone --depth 1 https://git.lighttpd.net/lighttpd/lighttpd1.4.git
cd lighttpd1.4
./autogen.sh
./configure
make install
```

Copy systemd unit file
[/etc/systemd/system/lighttpd.service](rootfs/etc/systemd/system/lighttpd.service).

Create log dir:

```bash
install -d -o root -g lighttpd -m 770 /var/log/lighttpd
```

And copy config into [/usr/local/etc/lighttpd](rootfs/usr/local/etc/lighttpd).
Finally enable and start it:

```bash
systemctl daemon-reload
systemctl enable lighttpd --now
```

### Wordpress

```bash
cd /srv/www/htdocs
mkdir wp
chmod -R u=rXw,g=rX,o=rX /srv/www
setfacl -dm u:lighttpd:rXw wp
setfacl -m u:lighttpd:rXw wp
git clone --depth 1 --branch 6.0-branch https://github.com/WordPress/WordPress.git wp
restorecon -R /srv/www
```

Now it should be accessible over HTTP.

### FTP

Copy configuration into [/etc/vsftpd/Born2beroot.conf](rootfs/etc/vsftpd/Born2beroot.conf),
and then create FTP dir, enable and start daemon:

```bash
mkdir /srv/ftp
echo "Hola, món!" > /srv/ftp/hola.txt
systemctl enable vsftpd@Born2beroot
systemctl enable vsftpd.target --now
```

### Firewall

Enable the firewall with `ufw enable`, delete all rules with successive calls
to `ufw delete 1` and the add the following ones:

```bash
# SSH:
ufw allow in 4242/tcp
# HTTP
ufw allow in 80/tcp
# FTP
ufw allow in 21/tcp
ufw allow in 21000:21999/tcp
```

### Mandatory Access Control

Make sure all files have the right SELinux context:

```bash
restorecon -R /
```
